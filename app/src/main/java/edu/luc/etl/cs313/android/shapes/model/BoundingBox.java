package edu.luc.etl.cs313.android.shapes.model;
import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {

		final Shape shape = f.getShape();
		return shape.accept(this);
	}
	@Override
	public Location onGroup(final Group g) {

		List<? extends Shape> shapeList = g.getShapes();
		Location LargeBox = shapeList.get(0).accept(this);
		Rectangle rec = (Rectangle) LargeBox.getShape();

		int minX = LargeBox.getX();
		int minY = LargeBox.getY();
		int maxX = minX + rec.getWidth();
		int maxY = minY + rec.getHeight();

		for (int i =1; i < shapeList.size(); i++) {
			Location smallerBox = shapeList.get(i).accept(this);
			Rectangle nextRec = (Rectangle) smallerBox.getShape();
			int minNextX = smallerBox.getX();
			int minNextY = smallerBox.getY();
			int maxNextX = minNextX + nextRec.getWidth();
			int maxNextY = minNextY + nextRec.getHeight();

			if (minNextX < minX)
			{
				minX = minNextX;
			}

			if (minNextY < minY)
			{
				minY = minNextY;
			}

			if (maxX < maxNextX)
			{
				maxX = maxNextX;
			}

			if (maxY < maxNextY)
			{
				maxY = maxNextY;
			}
		}

		LargeBox = new Location(minX, minY, new Rectangle(maxX - minX, maxY - minY));
		return LargeBox;
	}

	@Override
	public Location onLocation(final Location l) {

		final int x = l.getX();
		final int y = l.getY();
		Shape sh = l.getShape();
		Location newL = sh.accept(this);

		return new Location(newL.getX() + l.getX(), newL.getY() + l.getY(), newL.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {

		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0, 0, new Rectangle(width, height));
	}


	@Override
	public Location onStroke(final StrokeColor c) {
		final int color = c.getColor();
		final Shape Shape = c.getShape();
		return Shape.accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {

		final Shape Shape = o.getShape();
		return Shape.accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {//need to check with professor on this
		return onGroup(s);

	}
}