package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME
		this.paint = paint; // FIXME
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final StrokeColor c) {

		int color = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(color);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style fill = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(fill);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {

		for(Shape s: g.getShapes()){
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {

		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {

		canvas.drawRect(0,0,r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {

		final Style style = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(style);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		int size = s.getPoints().size();
		int floatSize = 4*size;
		final float[] pts = new float[floatSize];
		for (int i = 0; i < size; i++)
		{
			pts[4*i] = s.getPoints().get(i).getX();
			pts[4*i+1] = s.getPoints().get(i).getY();
			pts[4*i+2] = s.getPoints().get((i+1)%size).getX();
			pts[4*i+3] = s.getPoints().get((i+1)%size).getY();
		}

		canvas.drawLines(pts, paint);
		return null;
	}
}
